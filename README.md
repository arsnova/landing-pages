# landing-pages

App landing pages for 

* [THM.cards](https://arsnova.cards) -> [cards.arsnova.rocks](https://cards.arsnova.rocks), 
* [Linux.cards](https://linux.cards) -> [linux.arsnova.rocks](https://linux.arsnova.rocks), 
* [arsnova.click](https://arsnova.click) -> [click.arsnova.rocks](https://click.arsnova.rocks), 
* [frag.jetzt](https://frag.jetzt) -> [jetzt.arsnova.rocks](https://jetzt.arsnova.rocks) and 
* [arsnova.eu](https://arsnova.eu) -> [arsnova.rocks](https://arsnova.rocks).