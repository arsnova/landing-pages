
let Swipe=class{

	/**
	 * 
	 * @param {HTMLElement} element
	 * @param {number} parallaxMod
	 */
	constructor(element,parallaxMod){
		
		const mod=(i,l)=>((i%l)+l)%l;
		
		let animationTime=200;
		/**
		 *
		 * @type {HTMLElement[]}
		 */
		let containerList=Array.from(
				element.getElementsByClassName('swipe-container')
		);
		containerList.forEach(e=>e.style.display='none');
		let length=containerList.length;
		const controller=new SwipeController(length);
		this._controller=controller;
		let index=0;
		let setIndex=i=>{
			index=mod(i,length);
			controller._changeObserver.set(index);
		};
		let l=new MouseListenerSupport(element,true,false);
		/**
		 *
		 * @param {function(c:HTMLElement)} c
		 */
		let prevContainer=c=>{
			if(index>0)c(containerList[index-1]);
			else c(containerList[length-1]);
		};

		/**
		 *
		 * @param {function(c:HTMLElement)} c
		 */
		let nextContainer=c=>{
			if(index<length-1)c(containerList[index+1]);
			else c(containerList[0]);
		};

		/**
		 *
		 * @param {function(c:HTMLElement)} c
		 */
		let container=c=>c(containerList[index]);

		/**
		 *
		 * @param {number} i
		 * @return {function({function(c:HTMLElement)})} c
		 */
		let getContainer=i=>c=>c(containerList[i]);

		/**
		 *
		 * @param {function(function(c:HTMLElement))} elem
		 * @param {function(e:HTMLElement,i:number,ar:HTMLElement[])} l
		 */
		let layerIterator=(elem,l)=>
				elem(c=>Array.from(c.getElementsByClassName('swipe-layer'))
						.forEach(l));

		/**
		 *
		 * @param {function(function(c:HTMLElement))} elem
		 * @param {number} offset
		 */
		let layerTranslate=(elem,offset)=>
				layerIterator(elem,(e,i,ar)=>
						translate(offset*-((ar.length-i)*parallaxMod),0)(e));

		/**
		 *
		 * @param {string} k
		 * @param {string} v
		 * @returns {function(c:HTMLElement)}
		 */
		let setStyle=(k,v)=>c=>c.style[k]=v;

		/**
		 *
		 * @param {number} x
		 * @param {number} y
		 * @returns {function(HTMLElement)}
		 */
		let translate=(x,y)=>
				setStyle('transform','translate3D('+x+'px, '+y+'px, 0)');

		/**
		 *
		 * @param {boolean} b
		 * @returns {function(HTMLElement)}
		 */
		let display=b=>setStyle('display',b?'block':'none');

		/**
		 *
		 * @param {boolean} b
		 * @returns {function(HTMLElement)}
		 */
		let transition=b=>
				setStyle('transition',b?'transform 0.2s ease-in-out':'none');

		let blocked=false;
		container(display(true));

		/**
		 *
		 * @param {function({function(c:HTMLElement)})} src
		 * @param {function({function(c:HTMLElement)})} dst
		 * @param {function()} action
		 * @param {number} mod
		 */
		let slide=(src,dst,action,mod)=>{
			blocked=true;
			dst(translate(element.offsetWidth*mod,0));
			dst(display(true));
			src(transition(true));
			layerIterator(src,transition(true));
			dst(transition(true));
			layerIterator(dst,transition(true));
			src(translate(-element.offsetWidth*mod,0));
			layerTranslate(src,-element.offsetWidth*mod);
			dst(translate(0,0));
			layerTranslate(dst,0);
			setTimeout(()=>{
				src(transition(false));
				layerIterator(src,transition(false));
				dst(transition(false));
				layerIterator(dst,transition(false));
				src(display(false));
				action();
			},animationTime);
		};

		let update=f=>{
			blocked=true;
			switch(f){
				case 0:{
					nextContainer(display(false));
					container(transition(true));
					layerIterator(container,transition(true));
					prevContainer(transition(true));
					layerIterator(prevContainer,transition(true));
					container(translate(element.offsetWidth,0));
					layerTranslate(container,element.offsetWidth);
					prevContainer(translate(0,0));
					layerTranslate(prevContainer,0);
					setTimeout(()=>{
						container(transition(false));
						layerIterator(container,transition(false));
						prevContainer(transition(false));
						layerIterator(prevContainer,transition(false));
						container(display(false));
						setIndex(index-1);
						blocked=false;
					},animationTime);
				}break;
				case 1:{
					prevContainer(transition(true));
					layerIterator(prevContainer,transition(true));
					container(transition(true));
					layerIterator(container,transition(true));
					nextContainer(transition(true));
					layerIterator(nextContainer,transition(true));
					container(translate(0,0));
					layerTranslate(container,0);
					nextContainer(translate(element.offsetWidth,0));
					layerTranslate(nextContainer,element.offsetWidth);
					prevContainer(translate(-element.offsetWidth,0));
					layerTranslate(prevContainer,-element.offsetWidth);
					setTimeout(()=>{
						container(transition(false));
						layerIterator(container,transition(false));
						nextContainer(transition(false));
						layerIterator(nextContainer,transition(false));
						prevContainer(transition(false));
						layerIterator(prevContainer,transition(false));
						nextContainer(display(false));
						prevContainer(display(false));
						blocked=false;
					},animationTime);
				}break;
				case 2:{
					prevContainer(display(false));
					container(transition(true));
					layerIterator(container,transition(true));
					nextContainer(transition(true));
					layerIterator(nextContainer,transition(true));
					container(translate(-element.offsetWidth,0));
					layerTranslate(container,-element.offsetWidth);
					nextContainer(translate(0,0));
					layerTranslate(nextContainer,0);
					setTimeout(()=>{
						container(transition(false));
						layerIterator(container,transition(false));
						nextContainer(transition(false));
						layerIterator(nextContainer,transition(false));
						container(display(false));
						setIndex(index+1);
						blocked=false;
					},animationTime);
				}break;
				default:break;
			}
		};

		l.onPress(start=>{
			if(blocked)return;
			l.onDragStart(()=>{
				let width=element.offsetWidth;
				prevContainer(display(true));
				prevContainer(translate(-width,0));
				layerTranslate(prevContainer,-width);
				nextContainer(display(true));
				nextContainer(translate(width,0));
				layerTranslate(nextContainer,width);
				l.onDragging(drag=>{
					let offset=drag.screenX-start.screenX;
					container(translate(offset,0));
					layerTranslate(container,offset);
					if(offset<0){
						nextContainer(translate(offset+width,0));
						layerTranslate(nextContainer,offset+width);
						prevContainer(translate(offset-width,0));
					}
					else{
						prevContainer(translate(offset-width,0));
						layerTranslate(prevContainer,offset-width);
						nextContainer(translate(offset+width,0));
					}
				});
				l.onRelease(drop=>{
					let offset=drop.screenX-start.screenX;
					if(offset<-50){
						update(2);
					}
					else if(offset>50){
						update(0);
					}
					else{
						update(1);
					}
				});
			});
			l.onClickRelease(click=>{
			});
		});

		/**
		 *
		 * @param {number} dst
		 * @param {function()} action
		 */
		let requestFrame=(dst,action)=>
				slide(container,getContainer(dst),action,controller.getIndex()>dst?-1:1);
		
		controller.onSlideRequest(dst=>{
				slide(
						container,
						getContainer(mod(index+dst,length)),
						()=>{
							setIndex(index+dst);
							blocked=false;
						},dst);
		});

		controller._requestObserver.subscribe(i=>{
			if(controller.getIndex()!==i){
				requestFrame(i,()=>{
					setIndex(i);
					blocked=false;
				});
			}
		});
	}

	/**
	 * 
	 * @return {SwipeController}
	 */
	getController(){
		return this._controller;
	}
	
};

/**
 * 
 * @type {MouseListenerSupport}
 */
let MouseListenerSupport=class{

	/**
	 * 
	 * @type {function(start:MouseEvent)[]}
	 */
	_onPressListener=[];
	_onDragStartListener=[];
	_onDraggingListener=[];
	_onReleaseListener=[];
	_onClickReleaseListener=[];
	/**
	 * 
	 * @type {boolean}
	 */
	_active=false;
	/**
	 * 
	 * @type {boolean}
	 */
	_xLock=false;
	/**
	 * 
	 * @type {boolean}
	 */
	_yLock=false;
	/**
	 * @type {HTMLElement}
	 */
	_element=undefined;
	/**
	 * @type {boolean}
	 */
	_isDragging=false;
	/**
	 * @type {boolean}
	 */
	_isLeaveLock=false;
	/**
	 * @type {function(e:MouseEvent|TouchEvent)}
	 */
	_move=undefined;
	/**
	 * @type {function(e:MouseEvent|TouchEvent)}
	 */
	_leave=undefined;
	/**
	 * @type {boolean}
	 */
	_isTouch=false;

	/**
	 * 
	 * @type {function(start:MouseEvent|TouchEvent,evt:MouseEvent|TouchEvent)}
	 */
	_validateDrag=undefined;
	
	/**
	 * @param {HTMLElement} element
	 * @param {boolean} xLock - true = only recognize horizontal drag
	 * @param {boolean} yLock - true = '' vertical
	 */
	constructor(element,xLock,yLock){
		this._xLock=xLock;
		this._yLock=yLock;
		this._element=element;
		let createValidateDrag=(dragCondition,leaveCondition)=>{
			if(typeof leaveCondition === 'undefined'){
				return (start,evt)=>{
					if(dragCondition(start,evt)){
						this._isDragging=true;
						this._onDragStartListener.forEach(l=>l());
					}
				};
			}
			else{
				return (start,evt)=>{
					if(dragCondition(start,evt)){
						this._isDragging=true;
						this._onDragStartListener.forEach(l=>l());
					}
					else if(leaveCondition(start,evt)){
						this._isLeaveLock=true;
						this._leave(evt);
					}
				};
			}
		};
		if(xLock){
			this._validateDrag=createValidateDrag(
					MouseListenerSupport.distanceX,MouseListenerSupport.distanceY);
		}
		else if(yLock){
			this._validateDrag=createValidateDrag(
					MouseListenerSupport.distanceY,MouseListenerSupport.distanceX);
		}
		else{
			this._validateDrag=createValidateDrag(MouseListenerSupport.distance);
		}
		element.addEventListener('mousedown',evt=>{
			if(this._active)return;
			this._isTouch=false;
			this.mousePressed(evt);
		});
		element.addEventListener('touchstart',evt=>{
			if(this._active)return;
			this._isTouch=true;
			this.mousePressed(evt);
		});
	}

	/**
	 * 
	 * @type {number}
	 */
	static distanceTrigger=15;

	/**
	 *
	 * @param {MouseEvent} a
	 * @param {MouseEvent} b
	 */
	static distance=(a,b)=>
			MouseListenerSupport.distanceX(a,b)
			||MouseListenerSupport.distanceY(a,b);

	/**
	 *
	 * @param {MouseEvent} a
	 * @param {MouseEvent} b
	 */
	static distanceX=(a,b)=>
			MouseListenerSupport.dif(a.screenX,b.screenX)
			>MouseListenerSupport.distanceTrigger;

	/**
	 *
	 * @param {MouseEvent} a
	 * @param {MouseEvent} b
	 */
	static distanceY=(a,b)=>
			MouseListenerSupport.dif(a.screenY,b.screenY)
			>MouseListenerSupport.distanceTrigger;
	
	/**
	 * 
	 * @param a
	 * @param b
	 */
	static dif=(a,b)=>a>b?a-b:b-a;

	/**
	 * @param {MouseEvent|TouchEvent} evt
	 */
	static convertEvent(evt){
		if(evt.type.startsWith('touch')){
			evt.screenX=evt.changedTouches[0].screenX;
			evt.screenY=evt.changedTouches[0].screenY;
		}
	}

	/**
	 * 
	 * @param {MouseEvent|TouchEvent} start
	 */
	mousePressed(start){
		MouseListenerSupport.convertEvent(start);
		if(this._active)return;
		this._active=true;
		this._onPressListener.forEach(l=>l(start));
		this._move=e=>this._mouseMoved(start,e);
		this._leave=e=>{
			this._mouseReleased(start,e);
			this._element.removeEventListener('mousemove',this._move);
			this._element.removeEventListener('touchmove',this._move);
			this._element.removeEventListener('mouseup',this._leave);
			this._element.removeEventListener('touchend',this._leave);
			this._element.removeEventListener('touchcancel',this._leave);
			this._element.removeEventListener('mouseleave',this._leave);
		};
		this._element.addEventListener('mousemove',this._move);
		this._element.addEventListener('touchmove',this._move);
		this._element.addEventListener('mouseup',this._leave);
		this._element.addEventListener('touchend',this._leave);
		this._element.addEventListener('touchcancel',this._leave);
		this._element.addEventListener('mouseleave',this._leave);
	}

	/**
	 * 
	 * @param {MouseEvent|TouchEvent} start
	 * @param {MouseEvent|TouchEvent} evt
	 */
	_mouseMoved(start,evt){
		MouseListenerSupport.convertEvent(evt);
		if(!this._isTouch&&evt.buttons===0){
			this._leave(evt);
		}
		else{
			if(this._isDragging){
				this._onDraggingListener.forEach(l=>l(evt));
			}
			else{
				this._validateDrag(start,evt);
			}
		}
	}

	/**
	 * @param {MouseEvent|TouchEvent} start
	 * @param {MouseEvent|TouchEvent} evt
	 */
	_mouseReleased(start,evt){
		MouseListenerSupport.convertEvent(evt);
		if(!this._isDragging&&!this._isLeaveLock){
			this._onClickReleaseListener.forEach(l=>l(evt));
		}
		this._onReleaseListener.forEach(l=>l(evt));
		this._onDragStartListener=[];
		this._onDraggingListener=[];
		this._onReleaseListener=[];
		this._onClickReleaseListener=[];
		this._isDragging=false;
		this._active=false;
		this._isLeaveLock=false;
	}

	/**
	 * 
	 * @param {function(start:MouseEvent)} start
	 */
	onPress(start){
		this._onPressListener.push(start);
	}

	/**
	 * 
	 * @param {function()} action
	 */
	onDragStart(action){
		this._onDragStartListener.push(action);
	}

	/**
	 * 
	 * @param {function(drag:MouseEvent)} drag
	 */
	onDragging(drag){
		this._onDraggingListener.push(drag);
	}

	/**
	 *
	 * @param {function(drop:MouseEvent)} drop
	 */
	onRelease(drop){
		this._onReleaseListener.push(drop);
	}

	/**
	 *
	 * @param {function(click:MouseEvent)} click
	 */
	onClickRelease(click){
		this._onClickReleaseListener.push(click);
	}
	
};

let SwipeObserver=class{
	
	_value;
	/**
	 * @type {function(value:*)[]}
	 */
	_listener;

	/**
	 * 
	 * @param {*?} initial
	 */
	constructor(initial){
		this._listener=[];
		this._value=initial;
	}
	
	get(){
		return this._value;
	}

	set(value){
		if(this._value!==value){
			this._value=value;
			this._listener.forEach(e=>e(this._value));
		}
	}

	/**
	 * 
	 * @param {*} l
	 * @param {boolean?} run
	 * @return {function()}
	 */
	subscribe(l,run){
		this._listener.push(l);
		if(run)l(this.get());
		return ()=>this._listener.splice(this._listener.indexOf(l),1);
	}
	
};

let SwipeController=class{

	/**
	 * @type {SwipeObserver}
	 */
	_changeObserver;
	
	/**
	 * @type {SwipeObserver}
	 */
	_requestObserver;

	/**
	 * @type {function(i:number)[]}
	 */
	_onSlideRequestListener;
	
	constructor(){
		this._changeObserver=new SwipeObserver(0);
		this._requestObserver=new SwipeObserver(0);
		this._onSlideRequestListener=[];
		this._changeObserver.subscribe(l=>{
			this._requestObserver._value=l;
		});
	}

	/**
	 * @return {SwipeObserver}
	 */
	getObserver(){
		return this._changeObserver;
	}

	/**
	 *
	 * @param {*} l
	 * @return {function()}
	 */
	subscribe(l){
		return this._changeObserver.subscribe(l);
	}

	/**
	 * 
	 * @param {number} i
	 */
	requestIndex(i){
		this._requestObserver.set(i);
	}
	
	requestNext(){
		this._onSlideRequestListener.forEach(e=>e(1));
	}
	
	requestPrev(){
		this._onSlideRequestListener.forEach(e=>e(-1));
	}

	/**
	 * 
	 * @param {function(i:number)} i
	 */
	onSlideRequest(i){
		this._onSlideRequestListener.push(i);
	}

	/**
	 * 
	 * @return {SwipePipSupport}
	 */
	createPipSupport(){
		return new SwipePipSupport(this);
	}

	/**
	 * 
	 * @return {number}
	 */
	getIndex(){
		return this._changeObserver.get();
	}
	
};

let SwipePipSupport=class{
	
	/**
	 * 
	 * @param {SwipeController} controller
	 */
	constructor(controller){
		this._controller=controller;
		this._map={};
	}

	/**
	 * 
	 * @param {HTMLButtonElement} button
	 * @param {number} index
	 * @return {SwipePip}
	 */
	addButton(button,index){
		const pip=new SwipePip(index,()=>{
			this._controller.requestIndex(index);
		});
		this._controller._changeObserver.subscribe(i=>{
			pip.getObserver().set(i===index);
		},true);
		this._map[index+'']=pip;
		return pip;
	}
	
};

let SwipePip=class{
	
	/**
	 *
	 * @param {number} index
	 * @param {function()} action 
	 */
	constructor(index,action){
		this._index=index;
		this._observer=new SwipeObserver(false);
		this._action=action;
	}

	/**
	 * 
	 * @return {SwipeObserver}
	 */
	getObserver(){
		return this._observer;
	}

	/**
	 * 
	 * @return {number}
	 */
	getIndex(){
		return this._index;
	}

	/**
	 * 
	 */
	request(){
		this._action();
	}
	
};











