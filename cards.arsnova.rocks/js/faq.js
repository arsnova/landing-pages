const items = document.querySelectorAll(".accordion button");

function toggleAccordion() {
    const itemToggle = this.getAttribute('aria-expanded');

    for (i = 0; i < items.length; i++) {
        items[i].setAttribute('aria-expanded', 'false');
    }

    if (itemToggle == 'false') {
        this.setAttribute('aria-expanded', 'true');
    }
}

items.forEach(item => item.addEventListener('click', toggleAccordion));

// DSGVO embedded YouTube Videos

(function($) {
    $( document ).ready( function() {
        if( $( '.video_wrapper' ).length > 0 ) {
            $( '.video_wrapper' ).each( function() {
                _wrapper = $( this );
                _wrapper.children( '.video_trigger' ).children( 'input[type="button"]' ).click( function() {
                    var _trigger = $( this ).parent();
                    _trigger.hide();
                    _trigger.siblings( '.video_layer' ).show().children( 'iframe' ).attr( 'src', 'https://www.youtube-nocookie.com/embed/' + _trigger.attr( 'data-source' ) + '?rel=0&controls=1&showinfo=0&autoplay=1&mute=0' );
                });
            });
        }
    });
})(jQuery);
