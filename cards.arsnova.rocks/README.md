🍅 cards Landingpage
---

The 🍅cards landing page is intended to provide the visitor with information and details about the properties and features of the 🍅cards platform.

We develop the 🍅cards Landingpage with the framework Bootstrap and orientate ourselves at the Bootstrap New Age Template.
- - - 

To run this project use:

    npm install 
    
    npm start

a new browser tab should open, if not click [this](http://localhost:3000/)