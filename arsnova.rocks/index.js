$(document).ready(function () {
    var pagePositon = 0,
        sectionsSeclector = 'section',
        $scrollItems = $(sectionsSeclector),
        offsetTolorence = 30,
        pageMaxPosition = $scrollItems.length - 1;

    //Map the sections:
    $scrollItems.each(function(index,ele) { $(ele).attr("debog",index).data("pos",index); });

    // Bind to scroll
    $(window).bind('scroll load',upPos);

    //Move on click:
    $('#arrow a').click(function(e){
        // scroll down one section
        if ($(this).hasClass('next') && pagePositon+1 <= pageMaxPosition) {
            pagePositon++;
            $('html, body').stop().animate({
                scrollTop: $scrollItems.eq(pagePositon).offset().top
            }, 800);
        }
        // scroll up one section
        if ($(this).hasClass('prev') && pagePositon-1 >= 0) {
            pagePositon--;
            $('html, body').stop().animate({
                scrollTop: $scrollItems.eq(pagePositon).offset().top
            }, 800);
            return false;
        }

        // scroll back to top
        if ($(this).hasClass('backtop') && pagePositon-1 >= 0) {
            pagePositon = 0;
            $('html, body').stop().animate({
                scrollTop: 0
            }, 800);
            return false;
        }
    });

    //Update position func:
    function upPos(){
        var fromTop = $(this).scrollTop();
        var $cur = null;
        var totalCur = $('section').length-1;


        $scrollItems.each(function(index,ele){
            if ($(ele).offset().top < fromTop + offsetTolorence) $cur = $(ele);
        });

        if ($cur != null && pagePositon != $cur.data('pos')) {
            pagePositon = $cur.data('pos');

            // if last item, add class to controls
            var current = $($cur).attr("debog");

            if ( current == totalCur ) {
                $('#arrow a').removeClass('next');
                $('#arrow a').addClass('backtop');
            } else {
                $('#arrow a').addClass('next');
                $('#arrow a').removeClass('backtop');
            };

        }
    }
});
