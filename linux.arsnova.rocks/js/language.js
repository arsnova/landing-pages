function setLanguage(lang) {
    if (lang === "de") {
        localStorage.setItem("lang", "de");
        document.documentElement.lang = lang;

    }

    if (lang === "en") {
        localStorage.setItem("lang", "en");
        document.documentElement.lang = lang;

    }

    else {
        localStorage.setItem("lang", "de");
        document.documentElement.lang = lang;


    }
}

