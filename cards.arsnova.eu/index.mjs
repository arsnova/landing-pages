import { LocaleService } from './app/services/localeService.mjs';
import i18n from './app/i18n.config.mjs';
const localeService = new LocaleService(i18n);
console.log(localeService.getLocales()); // ['en', 'el']
console.log(localeService.getCurrentLocale()); // 'en'
console.log(localeService.translate('Hallo')); //  'Hello'
console.log(localeService.translatePlurals('You have %s message', 3));
